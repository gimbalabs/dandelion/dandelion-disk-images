# README

- dandelion-worker-x86_64.img: raw disk image. You can `dd` this to your physical disk, but it contains a 2gb only partition that you will need to resize accordingly to your disk size.
- dandelion-worker-x86_64.qcow2: qcow2 image containing a 200gb virtual disk. You can `dd` it directly to your 200gb+ disk with:
```
qemu-img dd -f qcow2 -O raw if=dandelion-worker-x86_64.qcow2 of=/dev/_CHANGEME_
```
- dandelion-worker-x86_64.vdi: You can use this one to boot from VirtualBox. Note that dandelion for testnet AND maninnet runs here, so you'll need ~32gb of RAM on your virtual machine :)

Once booted, you can login into them using the following credentials:

- User:     `root`
- Password: `CHANGEME`

Note that SSH is available but has `root` login disabled, so you'd have to a) enable it or b) authorize your own SSH public keys.

Once logged in, you'll be able to execute `kubectl get pods -A` et al to check your Dandelion deployment :)
